<!-- README.md is generated from README.Rmd. Please edit that file -->

# smurf

[![CRAN\_Status\_Badge](https://www.r-pkg.org/badges/version/smurf)](https://cran.r-project.org/package=smurf)
[![pipeline](https://gitlab.com/TReynkens/smurf/badges/master/build.svg)](https://gitlab.com/TReynkens/smurf/pipelines)
[![coverage
report](https://gitlab.com/TReynkens/smurf/badges/master/coverage.svg)](https://gitlab.com/TReynkens/smurf/commits/master)

## Overview

*smurf* contains the implementation of the SMuRF (Sparse Multi-type
Regularized Feature modeling; see Devriendt et al., 2018) algorithm to
fit generalized linear models (GLMs) with multiple types of predictors
via regularized maximum likelihood. Next to the fitting procedure,
following functionality is available:

  - Selection of the regularization tuning parameter `lambda` using
    three different approaches: in-sample, out-of-sample or using
    cross-validation.
  - S3 methods to handle the fitted object including visualization of
    the coefficients and a model summary.

## Documentation

More details on how to use the package functions can be found in the
vignette or in the package documentation.

## Installation

The *smurf* package can be installed from CRAN.

You can also install the latest development version of *smurf* from
GitLab. If you work on Windows, make sure first that
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) is installed.
Then, install the latest development version of *smurf* using

    install.packages("remotes")
    
    remotes::install_git("https://gitlab.com/TReynkens/smurf.git")

## Authors

Tom Reynkens, Sander Devriendt and Katrien Antonio

## Reference

Devriendt, S., Antonio, K., Reynkens, T. and Verbelen, R. (2018).
“Sparse Regression with Multi-type Regularized Feature Modeling.”
*arXiv:1810.03136*.
